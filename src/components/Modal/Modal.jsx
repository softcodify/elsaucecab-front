import React from "react"
import './Modal.css'

function Modal({ children }) {
  function handlerCloseModal() {
    document.querySelector('body').classList.remove('bodyScroll')
    document.querySelector('.modal').classList.add('hiddenModal')
  }

  return (
    <>
      <div className="modal hiddenModal">
        <section className="modal-container container my-5 p-4">
          <i onClick={handlerCloseModal} className="btn-close bi bi-x-lg"></i>
          {children}
        </section>
      </div>
    </>
  )
}

export default Modal
