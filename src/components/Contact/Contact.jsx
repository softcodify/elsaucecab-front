import React from "react"
import './Contact.css'

function Contact() {
  return (
    <>
      <h2 className="mb-3">Contacto</h2>
      <div className="divider"></div>
      <p className="mt-4">¡Gracias por elegir nuestras cabañas para tu escapada! Estamos aquí para ayudarte en todo lo que necesites. También puedes seguirnos en nuestras redes sociales y encuentra nuestras cabañas en otros sitios populares. No dudes en contactarnos.</p>

      <div className="contact-container">
        <div className="btn-contact-container my-5">
          <div className="header-contact">
            <h4>Natalia</h4>
            <small>3542 62-0192</small>
          </div>
          <a className="btn-contact" href="https://wa.me/5493542620192" target="_blank">
            <i className="bi bi-whatsapp"></i>
          </a>
          <a className="btn-contact" href="tel:+5493542620192" target="_blank">
            <i className="bi bi-telephone"></i>
          </a>
        </div>

        <div className="btn-contact-container my-5">
          <div className="header-contact">
            <h4>Gonzalo</h4>
            <small>3542 62-0537</small>
          </div>
          <a className="btn-contact" href="https://wa.me/5493542620537" target="_blank">
            <i className="bi bi-whatsapp"></i>
          </a>
          <a className="btn-contact" href="tel:+5493542620537" target="_blank">
            <i className="bi bi-telephone"></i>
          </a>
        </div>

        <div className="btn-contact-container my-5">
          <div className="header-contact">
            <h4>Páginas</h4>
          </div>
          <a className="btn-contact" href="https://www.instagram.com/cabanas.elsauce/?igshid=YmMyMTA2M2Y=" target="_blank">
            <i className="bi bi-instagram"></i>
          </a>
          <a className="btn-contact" href="https://www.alquilerargentina.com/alojamientos/kf60-Caba%C3%B1a-Caba%C3%B1as-El-Sauce-Mina-Clavero.html" target="_blank">
            <img src="alquiler-arg.svg" alt="" />
          </a>
          <a className="btn-contact">
            <img src="airbnb.svg" alt="" />
          </a>
        </div>
      </div>

      <p className="text-center mb-5">¡Esperamos que tengas una estancia encantadora en nuestras cabañas!</p>

      <h4 className="mt-auto mb-4">Ubicación</h4>
      <div className="map-container">

        <iframe className="google-maps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3393.5845615348885!2d-65.019821!3d-31.7272415!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x942d2666173de101%3A0x1d3fe74b6116ef2b!2sAnastasia%20Fabre%20de%20Merlo%20%26%20Jose%20Vila%20Guerrero%2C%20Mina%20Clavero%2C%20C%C3%B3rdoba!5e0!3m2!1ses-419!2sar!4v1702691121084!5m2!1ses-419!2sar" loading="lazy"></iframe>
      </div>
    </>
  )
}

export default Contact
