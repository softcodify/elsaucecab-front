import React from "react"
import './Background.css'

function Background() {
  return (
    <>
      <div className="image-container">
        <img className="bg-image" src="fondo.jpg" alt="" />
      </div>
    </>
  )
}

export default Background
