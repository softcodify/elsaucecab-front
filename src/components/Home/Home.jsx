import React from "react"
import Background from "../Background/Background"

import './Home.css'

function Home() {
  return (
    <>
      <Background />
      <main className="container mt-5 gap-5">
        <div className="home-container gap-4 p-5">
          <div className="home-text-container p-4">
            <h2>CABAÑAS EL SAUCE</h2>
            <small className="slogan mb-5">Tranquilidad y calidez, transforman los sentidos de tu estadía</small>
            <div className="text-container gap-3">
              <p className="text-card p-4">
                En la serenidad de Mina Clavero descubre un refugio acogedor que combina comodidad y encanto rústico. Nuestras cabañas, estratégicamente ubicadas, te ofrecen la oportunidad de escapar del bullicio urbano y sumergirte en la belleza tranquila de las sierras cordobesas.
                Si deseas relajarte y recargar energías en un entorno pacífico, nuestro complejo es el lugar perfecto para todas tus necesidades. Descubrí la auténtica hospitalidad cordobesa mientras te sumerges en una experiencia única en nuestro rincón de paz en Mina Clavero. ¡Te esperamos con los brazos abiertos!
              </p>
            </div>
            <a href="#" className="mt-5 glass-btn btn-custom">Ir a las cabañas</a>
          </div>
          <img className="logo-home" src="logo-home.svg" alt="" />
        </div>

        <div>
          <div className="home-card p-5 gap-5">
            <img src="foto.jpg" alt="" />
            <div className="home-card-text">
              <h3 className="mb-3">NOSOTROS</h3>
              <div className="divider"></div>
              <p className="mt-4">Hola, somos Natalia y Gonzalo! Estamos para ayudar y asesorar en todo lo que necesiten nuestros visitantes. Queremos que su estadía sea placentera, brindándoles un servicio personalizado hasta a veces artesanal, para que tengan una experiencia única, y mejor aún, diferente. Para nosotros lejos está de ser un trabajo, porque dedicarle todo el amor a este proyecto se volvió en nuestro nuestro principal pasatiempo.</p>
              <small className="mt-5">Administración El Sauce</small>
            </div>
          </div>
        </div>
      </main>
    </>
  )
}

export default Home
