import React from "react"
import './Information.css'

function Information() {
  return (
    <>
      <section className="container my-5">
        <h2 className="mb-3">Información</h2>
        <div className="divider"></div>
        <div className="information mt-4 gap-4">
          <h4>EL COMPLEJO</h4>
          <p>El complejo está ubicado en zona estratégica para disfrutar placenteras vacaciones en familia. El mismo está situado en barrio residencial exclusivo de cabañas, a una cuadra y media del Río Los Sauces, y a 10 cuadras de la avenida principal. Se garantiza seguridad y tranquilidad en contacto con la naturaleza. El complejo tiene una antigüedad de más de 5 años, destacando la minuciosa y esmerada atención de sus dueños. Abierto todo el año.</p>
        </div>
      </section>
    </>
  )
}

export default Information
