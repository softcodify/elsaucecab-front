import React, { useEffect } from 'react'
import './App.css'
import { Routes, Route } from 'react-router-dom'
import { useLocation } from 'react-router-dom'

import Navbar from './components/Navbar/Navbar'

import Home from './components/Home/Home'
import Information from './components/Information/Information'
import Gallery from './components/Gallery/Gallery'
import Reservation from './components/Reservation/Reservation'
import Recommended from './components/Recommended/Recommended'
import News from './components/News/News'

import Footer from './components/Footer/Footer'

function App() {
  const location = useLocation()

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [location])
  return (
    <>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/information" element={<Information />} />
        <Route path="/gallery" element={<Gallery />} />
        <Route path="/reservation" element={<Reservation />} />
        <Route path="/recommended" element={<Recommended />} />
        <Route path="/news" element={<News />} />
        {/* <Route path="/contact" element={<Contact />} /> */}
      </Routes>
      
      <Footer />
    </>
  )
}

export default App
