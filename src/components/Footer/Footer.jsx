import React from "react"
import './Footer.css'

function Footer() {
  return (
    <>
      <footer className="">
        <div className="container py-5">
          <a href="https://softcodify.netlify.app/" target="_blank">
            <small>SoftCodify | 2023</small>
          </a>
        </div>
      </footer>
    </>
  )
}

export default Footer
