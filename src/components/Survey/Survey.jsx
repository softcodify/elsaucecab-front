import React, { useState } from "react"
import Swal from 'sweetalert2'
import './Survey.css'

import apiUrl from "../../api"

import axios from "axios"

function Survey() {
  const [showInput, setShowInput] = useState(false)

  async function sendSurvey(dataForm) {
    try {
      const response = await axios.post(`${apiUrl}/survey`, dataForm)
    } catch (error) {
      console.error('Error al enviar la solicitud POST:', error)
    }
  }

  function handleSubmit(event) {
    event.preventDefault()
    let formulario = document.querySelector("form")

    const formData = Object.fromEntries(new FormData(formulario))

    const dataSurvey = {
      alojamiento: {
        limpieza: formData.limpieza,
        equipamientoCumpleExpectativas: formData.equipamientoCumpleExpectativas,
        instalaciones: formData.instalaciones || "no se completó"
      },
      servicios: {
        calidadPersonal: formData.calidadPersonal,
        serviciosAdicionales: formData.serviciosAdicionales || "no se completó"
      },
      experienciaGeneral: {
        aspectosPositivos: formData.aspectosPositivos || "no se completó",
        aspectosMejorables: formData.aspectosMejorables || "no se completó",
      },
      probabilidadRecomendacion: formData.probabilidadRecomendacion,
      comentariosAdicionales: formData.comentariosAdicionales || "no se completó"
    }

    if (showInput == true) {
      let inputValue = document.querySelector('#equip4').value
      let regex = /\S/
      if (regex.test(inputValue)) {
        dataSurvey.alojamiento.equipamientoCumpleExpectativas = formData.equipamientoCumpleExpectativas + document.querySelector('#equip4').value
      } else {
        document.querySelector('.equip4').scrollIntoView({ behavior: 'smooth' })
      }
    }

    Swal.fire({
      color: "#33374c",
      background: "#e8e9ec",
      title: "¿Desea confirmar la encuesta?",
      confirmButtonText: "Enviar",
      confirmButtonColor: "#bf8f73",
      showCancelButton: true,
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        const Toast = Swal.mixin({
          toast: true,
          position: "top-end",
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.onmouseenter = Swal.stopTimer
            toast.onmouseleave = Swal.resumeTimer
          }
        })
        Toast.fire({
          title: "Encuesta enviada"
        })
        handleHiddenInput()
        sendSurvey(dataSurvey)
        formulario.reset()
      }
    })

  }

  function handleShowInput() {
    setShowInput(true)
    document.querySelector('#equip4').setAttribute("required", "true")
  }

  function handleHiddenInput() {
    setShowInput(false)
    document.querySelector('#equip4').value = ''
    document.querySelector('#equip4').removeAttribute("required")
  }
  return (
    <>
      <h2 className="mb-3">Encuesta</h2>
      <div className="divider"></div>
      <p className="mt-4">Gracias por elegir nuestro complejo de cabañas. Su opinión es crucial para mejorar nuestros servicios. Por favor, tómese unos minutos para completar nuestra breve Encuesta de Satisfacción. Sus comentarios anónimos nos ayudarán a garantizar una experiencia aún mejor para usted y futuros huéspedes. Agradecemos su participación. <em>Los campos con <span className="red"><strong>*</strong></span> son campos requeridos</em>.</p>
      
      <form onSubmit={handleSubmit} className="gap-3">
        <h3 className="mt-5">Alojamiento:</h3>

        <fieldset>
          <legend><span className="red"><strong>*</strong></span>a. Por favor, califique la limpieza de la cabaña.</legend>
          <div>
            <input type="radio" id="lim1" name="limpieza" value="Mala" required />
            <label htmlFor="lim1"> Mala</label>
          </div>
          <div>
            <input type="radio" id="lim2" name="limpieza" value="Regular" required />
            <label htmlFor="lim2"> Regular</label>
          </div>
          <div>
            <input type="radio" id="lim3" name="limpieza" value="Buena" required />
            <label htmlFor="lim3"> Buena</label>
          </div>
          <div>
            <input type="radio" id="lim4" name="limpieza" value="Muy buena" required />
            <label htmlFor="lim4"> Muy buena</label>
          </div>
          <div>
            <input type="radio" id="lim5" name="limpieza" value="Excelente" required />
            <label htmlFor="lim5"> Excelente</label>
          </div>
        </fieldset>

        <fieldset>
          <legend><span className="red"><strong>*</strong></span>b. ¿El equipamiento de la cabaña cumplió con sus expectativas?</legend>
          <div>
            <input onClick={handleHiddenInput} type="radio" id="equip1" name="equipamientoCumpleExpectativas" value="Si" required />
            <label htmlFor="equip1"> Si</label>
          </div>
          <div>
            <input onClick={handleHiddenInput} type="radio" id="equip2" name="equipamientoCumpleExpectativas" value="No" required />
            <label htmlFor="equip2"> No</label>
          </div>
          <div>
            <input onClick={handleShowInput} type="radio" id="equip3" name="equipamientoCumpleExpectativas" value="En parte: " required />
            <label htmlFor="equip3"> En parte</label>
          </div>
          <div className={`equip4 ${showInput ? "" : "hiddenInput"}`}>
            <label htmlFor="equip4">Por favor, especifique: </label>
            <textarea className="mt-1" name="aclara" id="equip4" cols="3"></textarea>
          </div>
        </fieldset>

        <fieldset>
          <legend>c. ¿Qué opina las instalaciones?</legend>
          <div className="inst1">
            <textarea className="mt-1" id="inst1" cols="3" name="instalaciones"></textarea>
          </div>
        </fieldset>

        <h3 className="mt-5">Servicios:</h3>

        <fieldset>
          <legend><span className="red"><strong>*</strong></span>a. ¿Cómo calificaría la calidad del servicio proporcionado por el personal?</legend>
          <div>
            <input type="radio" id="cal1" name="calidadPersonal" value="Mala" required />
            <label htmlFor="cal1"> Mala</label>
          </div>
          <div>
            <input type="radio" id="cal2" name="calidadPersonal" value="Regular" required />
            <label htmlFor="cal2"> Regular</label>
          </div>
          <div>
            <input type="radio" id="cal3" name="calidadPersonal" value="Buena" required />
            <label htmlFor="cal3"> Buena</label>
          </div>
          <div>
            <input type="radio" id="cal4" name="calidadPersonal" value="Muy buena" required />
            <label htmlFor="cal4"> Muy buena</label>
          </div>
          <div>
            <input type="radio" id="cal5" name="calidadPersonal" value="Excelente" required />
            <label htmlFor="cal5"> Excelente</label>
          </div>
        </fieldset>

        <fieldset>
          <legend>b. ¿Hubo algún servicio adicional que le gustaría haber tenido?</legend>
          <div className="serv1">
            <label htmlFor="serv1">Por favor, describa: </label>
            <textarea className="mt-1" id="serv1" cols="1" name="serviciosAdicionales"></textarea>
          </div>
        </fieldset>

        <h3 className="mt-5">Experiencia General:</h3>

        <fieldset>
          <legend>a. ¿Qué aspectos le gustaron más de su estadía?</legend>
          <div className="pos1">
            <label htmlFor="pos1">Por favor, describa: </label>
            <textarea className="mt-1" id="pos1" cols="1" name="aspectosPositivos" ></textarea>
          </div>
        </fieldset>

        <fieldset>
          <legend>b. ¿Qué aspectos cree que podrían mejorarse?</legend>
          <div className="mej1">
            <label htmlFor="mej1">Por favor, describa: </label>
            <textarea className="mt-1" id="mej1" cols="1" name="aspectosMejorables" ></textarea>
          </div>
        </fieldset>

        <h3 className="mt-5">Probabilidad de Recomendación:</h3>

        <fieldset>
          <legend><span className="red"><strong>*</strong></span> ¿Con qué probabilidad recomendaría nuestro complejo de cabañas a amigos o familiares?</legend>
          <div>
            <input type="radio" id="reco1" name="probabilidadRecomendacion" value="No lo recomendaría" required />
            <label htmlFor="asp1"> No lo recomendaría</label>
          </div>
          <div>
            <input type="radio" id="reco2" name="probabilidadRecomendacion" value="Muy poco probable" required />
            <label htmlFor="asp2"> Muy poco probable</label>
          </div>
          <div>
            <input type="radio" id="reco3" name="probabilidadRecomendacion" value="Quizas lo recomendaría" required />
            <label htmlFor="asp3"> Quizás lo recomendaría</label>
          </div>
          <div>
            <input type="radio" id="reco4" name="probabilidadRecomendacion" value="Recomendable" required />
            <label htmlFor="asp4"> Recomendable</label>
          </div>
          <div>
            <input type="radio" id="reco5" name="probabilidadRecomendacion" value="Altamente recomendable" required />
            <label htmlFor="asp5"> Altamente recomendable</label>
          </div>
        </fieldset>

        <h3 className="mt-5">Comentarios Adicionales:</h3>

        <fieldset>
          <legend>Por favor, comparta cualquier comentario adicional, sugerencia o experiencia que desee compartir con nosotros.</legend>
          <div className="coment1">
            <label htmlFor="coment1"> </label>
            <textarea id="coment1" cols="1" name="comentariosAdicionales" ></textarea>
          </div>
        </fieldset>

        <p>¡Gracias por dedicar tiempo a completar nuestra encuesta! Su opinión es muy valiosa para mejorar nuestros servicios.</p>

        <input className="send-form btn-custom mt-4" type="submit" value="Confirmar" />
      </form>
    </>
  )
}

export default Survey

