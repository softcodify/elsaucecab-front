import React, { useState } from 'react'
import { NavLink, useLocation } from 'react-router-dom'

import './Navbar.css'

import Contact from '../Contact/Contact'
import Survey from '../Survey/Survey'
import Modal from '../Modal/Modal'

function Navbar() {
  const [isMenuOpen, setIsMenuOpen] = useState(false)

  const [isModalOpen, setIsModalOpen] = useState(false)

  const [comp, setComp] = useState(null)

  const showContact = () => {
    setComp(<Contact />)
  }

  const showSurvey = () => {
    setComp(<Survey />)
  }

  function handlerToggleMenu() {
    setIsMenuOpen(!isMenuOpen)
    document.querySelector('body').classList.toggle('bodyScroll')
    document.querySelector('.menu').classList.toggle('hiddenMenu')

    if (isMenuOpen) {
      document.querySelector('.blackout').classList.add('fade')
      setTimeout(() => {
        document.querySelector('.blackout').classList.add('hiddenBlackout')
      }, 300)
    } else {
      document.querySelector('.blackout').classList.remove('hiddenBlackout')
      setTimeout(() => {
        document.querySelector('.blackout').classList.remove('fade')
      }, 1)
    }
  }

  function handlerToggleModal() {
    setIsModalOpen(!isModalOpen)
    document.querySelector('body').classList.toggle('bodyScroll')
    document.querySelector('.modal').classList.toggle('hiddenModal')
  }

  const [navReveal, setNavReveal] = useState(false)
  const changeColor = () => {
    if (window.scrollY >= 50) {
      setNavReveal(true)
    } else {
      setNavReveal(false)
    }
  }

  window.addEventListener('scroll', changeColor)

  const location = useLocation()
  const notIsHome = location.pathname !== '/'
  
  return (
    <>
      <nav className={`container-fluid 
        ${navReveal && 'navReveal'} 
        ${notIsHome && 'navBackgorund'}`}
      >
        <h2 className="navbar-title">EL SAUCE</h2>
        <i onClick={handlerToggleMenu} className="navbar-icon bi bi-list"></i>
      </nav>

      <div onClick={handlerToggleMenu} className="blackout hiddenBlackout fade"></div>

      <ul className="menu hiddenMenu">

        <li className="menu-header">
          <h3 className="menu-title">MENÚ</h3>
          <i onClick={handlerToggleMenu} className="bi bi-x-lg"></i>
        </li>
        <li className="menu-container">
          <ul className="menu-list">
            <li>
              <NavLink onClick={handlerToggleMenu} to="/" className={`menu-item ${({isActive})=> isActive ? 'active' : ''}`}><i className="bi bi-house"></i> Home</NavLink>
            </li>
            <li>
              <NavLink onClick={handlerToggleMenu} to="/information" className="menu-item"><i className="bi bi-info-circle"></i> Información</NavLink>
            </li>
            <li>
              <NavLink onClick={handlerToggleMenu} to="/gallery" className="menu-item"><i className="bi bi-image"></i> Galería</NavLink>
            </li>
            <li>
              <NavLink onClick={handlerToggleMenu} to="/reservation" className="menu-item"><i className="bi bi-check-square"></i> Reserva</NavLink>
            </li>
            <li>
              <NavLink onClick={handlerToggleMenu} to="/recommended" className="menu-item"><i className="bi bi-star"></i> Recomendados</NavLink>
            </li>
            <li>
              <NavLink onClick={handlerToggleMenu} to="/news" className="menu-item"><i className="bi bi-newspaper"></i> Noticias</NavLink>
            </li>
            <li>
              <span onClick={() => {
                handlerToggleModal() 
                handlerToggleMenu()
                showContact()
              }} className="menu-item"><i className="bi bi-telephone-plus"></i> Contacto</span>
            </li>
            <li>
              <span onClick={() => {
                handlerToggleModal() 
                handlerToggleMenu()
                showSurvey()
              }} className="menu-item"><i className="bi bi-list-check"></i> Encuesta</span>
            </li>
          </ul>
        </li>
        {/* <li className="secondary-menu-container">
          <ul className="secondary-menu-list">  
          </ul>
        </li> */}
      </ul>

      <Modal children={comp}/>

    </>
  )
}

export default Navbar
